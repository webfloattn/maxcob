$(document).ready(function () {

//Nav
	function scrollToAnchor(aid){
	    var aTag = jQuery("a[name='"+ aid +"']");
	    jQuery('html,body').animate({scrollTop: aTag.offset().top},'slow');
	}

	$("#link2").click(function() {
	   scrollToAnchor('empresa');
	});
	$("#link3").click(function() {
	   scrollToAnchor('servicos');
	});
	$("#link4").click(function() {
	   scrollToAnchor('contato');
	});

//Botão Ligue-me
	Shadowbox.init({
		handleOversize: "drag",
		modal: true
	});
//Menu Serviços
	$("div.p2").find('p').hide();
	$("div.p3").find('p').hide();
	$("div.p4").find('p').hide();
	$("div.p5").find('p').hide();

	$(".p11").on("mouseover",function(){
		$("div.p1").find('p').fadeIn(1000);
		$("div.p2").find('p').hide();
		$("div.p3").find('p').hide();
		$("div.p4").find('p').hide();
		$("div.p5").find('p').hide();
		$('.p11').css({"background":"url(images/botao_servicos.png) no-repeat", "color":"#FFF"});
		$('.p22').css({"background":"none", "color":"#262626"});
		$('.p33').css({"background":"none", "color":"#262626"});
		$('.p44').css({"background":"none", "color":"#262626"});
		$('.p55').css({"background":"none", "color":"#262626"});
	});

	$(".p22").on("mouseover",function(){
		$("div.p1").find('p').hide();
		$("div.p2").find('p').fadeIn(1000);
		$("div.p3").find('p').hide();
		$("div.p4").find('p').hide();
		$("div.p5").find('p').hide();
		$('.p11').css({"background":"none", "color":"#262626"});
		$('.p22').css({"background":"url(images/botao_servicos.png) no-repeat", "color":"#FFF"});
		$('.p33').css({"background":"none", "color":"#262626"});
		$('.p44').css({"background":"none", "color":"#262626"});
		$('.p55').css({"background":"none", "color":"#262626"});
	});

	$(".p33").on("mouseover",function(){
		$("div.p1").find('p').hide();
		$("div.p2").find('p').hide();
		$("div.p3").find('p').fadeIn(1000);
		$("div.p4").find('p').hide();
		$("div.p5").find('p').hide();
		$('.p11').css({"background":"none", "color":"#262626"});
		$('.p22').css({"background":"none", "color":"#262626"});
		$('.p33').css({"background":"url(images/botao_servicos.png) no-repeat", "color":"#FFF"});
		$('.p44').css({"background":"none", "color":"#262626"});
		$('.p55').css({"background":"none", "color":"#262626"});
	});

	$(".p44").on("mouseover",function(){
		$("div.p1").find('p').hide();
		$("div.p2").find('p').hide();
		$("div.p3").find('p').hide();
		$("div.p4").find('p').fadeIn(1000);
		$("div.p5").find('p').hide();
		$('.p11').css({"background":"none", "color":"#262626"});
		$('.p22').css({"background":"none", "color":"#262626"});
		$('.p33').css({"background":"none", "color":"#262626"});
		$('.p44').css({"background":"url(images/botao_servicos.png) no-repeat", "color":"#FFF"});
		$('.p55').css({"background":"none", "color":"#262626"});
	});

	$(".p55").on("mouseover",function(){
		$("div.p1").find('p').hide();
		$("div.p2").find('p').hide();
		$("div.p3").find('p').hide();
		$("div.p4").find('p').hide();
		$("div.p5").find('p').fadeIn(1000);
		$('.p11').css({"background":"none", "color":"#262626"});
		$('.p22').css({"background":"none", "color":"#262626"});
		$('.p33').css({"background":"none", "color":"#262626"});
		$('.p44').css({"background":"none", "color":"#262626"});
		$('.p55').css({"background":"url(images/botao_servicos.png) no-repeat", "color":"#FFF"});
	});

	

// Botão voltarTopo
	$(".voltarTopo").hide();
	$(function () {
		$(window).scroll(function () {
			if ($(this).scrollTop() > 300) {
				$('.voltarTopo').fadeIn();
			} else {
				$('.voltarTopo').fadeOut();
			}
		});

        $('.voltarTopo').click(function() {
        	$('body,html').animate({scrollTop:0},600);
        }); 
	});
});